We provide physical therapy services in Monterey, Pacific Grove, Carmel, Marina, Seaside and surrounding areas. We help active people recover from injury, stay and get fit, move better, and make transformative life changes for long term results.

Address: 191 Lighthouse Ave, Ste B, Monterey, CA 93940, USA

Phone: 831-531-7177

Website: https://www.kadalystpt.com